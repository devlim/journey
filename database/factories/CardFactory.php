<?php

use Faker\Generator as Faker;
use App\Models\Card;

$factory->define(Card::class, function (Faker $faker) {
    return [
        'title' => $faker->text($maxNbChars = 200),
        'url' => $faker->url(),
        'pin' => $faker->boolean(mt_rand(0, 100)),
        'important' => $faker->boolean(mt_rand(0, 100)),
        'visit' => $faker->boolean(mt_rand(0, 2000)),
    ];
});