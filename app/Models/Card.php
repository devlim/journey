<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Supports\Scopes\UserScope;

class Card extends Model
{
    protected $attributes = [
        'pin' => 0,
        'important' => 0,
        'visit' => 0, //?
    ];

    protected $fillable = [
        'title',
        'url',
        'pin',
        'important',
        'user_id',
    ];

    protected $guarded = [
        'id',
        'vist',
        'created_at',
        'updated_at',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserScope);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}