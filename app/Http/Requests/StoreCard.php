<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreCard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->get('card') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:250',
            'url' => 'required|url|max:250',
            'pin' => 'boolean',
            'important' => 'boolean'
        ];
    }

    // https://stackoverflow.com/questions/46325449/how-i-can-return-a-customized-response-in-a-formrequest-class-in-laravel-5-5
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(
            response()->json(
                $validator->errors()->all(),
                422
            )
        );
    }
}
