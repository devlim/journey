<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function responseWithJsonError()
    {
        return response()->json(
            [
                'error' => 'RESOURCE_NOT_FOUND',
                'error_message' => 'Resource not found.',
            ],
            404
        );
    }
}
