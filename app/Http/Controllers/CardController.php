<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Card;
use App\Http\Resources\Card as CardResource;
use App\Http\Requests\StoreCard as StoreCardFormRequest;

class CardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Card::paginate(15);

        return CardResource::collection($model);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCardFormRequest $request)
    {
        $user_data = array_merge(
            $request->get('card'),
            [
                'user_id' => Auth::id()
            ]
        );
        $model = Card::create($user_data);

        return new CardResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Card::find($id);

        return new CardResource($model);
    }
    /*
    // Snippet #1: use with code in app/Exceptions/Handler.php
    public function show(Card $card)
    {
        //$model = Card::find($id);

        return new CardResource($card);
    }
    */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCardFormRequest $request, $id)
    {
        $card_input = $request->get('card');
        try {
            $model = Card::findOrFail($id);
        } catch (ModelNotFoundException $model) {
            return $this->responseWithJsonError();
        }
        $model->update($card_input);

        return new CardResource($model);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $model = Card::findOrFail($id);

            if ($model->delete()) {
                return new CardResource($model);
            }
        } catch (ModelNotFoundException $model) {
            return $this->responseWithJsonError();
        }
    }
}
