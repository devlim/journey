<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // handle api (a.k.a json response)
        /* Snippet #1:
         * desc: when resources not found, return error msg like following
         *       instead of empty data with 200 status
         * note: the request header must has: Accept: application/json.
        {
            "error": "RESOURCE_NOT_FOUND",
            "error_message": "Resource not found."
        }
        */
        /*
        if ($exception instanceof ModelNotFoundException && $request->wantsJson()) {
            return response()->json(
                [
                    'error' => 'RESOURCE_NOT_FOUND',
                    'error_message' => 'Resource not found.',
                ],
                404
            );
        }
        */

        // handle non-api (a.k.a hmtl response)
        return parent::render($request, $exception);
    }
}
